package si.ellj.arso.unmarshall;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.Charset;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import org.apache.log4j.Logger;

import si.ellj.arso.object.ArsoDataObject;
import si.ellj.arso.soap.SendWeatherESB;

public class XMLUnmarshaller {
	
	private static final Logger log = Logger.getLogger(XMLUnmarshaller.class);

	public static void convertToJava(String xmlString, String weatherType, String endpointAddress, String trustorePath, String trustorePassword, String keystorePath, String keystorePassword) {
		
		JAXBContext jaxbContext;
		
		try {
		    jaxbContext = JAXBContext.newInstance(ArsoDataObject.class);   

		    InputStream inputStream = new ByteArrayInputStream(xmlString.getBytes(Charset.forName("UTF-8")));
		    Reader reader = new InputStreamReader(inputStream, "UTF-8");
		    
		    Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
		    ArsoDataObject arsoDataObject = (ArsoDataObject) jaxbUnmarshaller.unmarshal(reader);
		    
		    //send parsed message to ESB
		    SendWeatherESB.sendWeatherToESB(arsoDataObject, weatherType, endpointAddress, trustorePath, trustorePassword, keystorePath, keystorePassword);
		     
		    log.info("Succesfully converted to Java object: " + arsoDataObject);
		} catch (Exception e) {
		    log.error("XML unmarshalling error: ", e);
		}
	}
}
