package si.ellj.arso.fetchdata;

import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.log4j.Logger;

import ch.iec.tc57._2018._7.weather_.ELEnvironmentalInformationKind;
import si.ellj.arso.files.CreateFolders;
import si.ellj.arso.unmarshall.XMLUnmarshaller;

public class FetchDataFromARSO {
	
	private static final Logger log = Logger.getLogger(FetchDataFromARSO.class);
	
	private static final List<String> skipFiles = new ArrayList(Arrays.asList(new String [] {".gitignore"}));
	
	/** config file for paths, usernames and passwords */
	private static final String CONFIG_PATH = "src/main/resources/config/arsoConfig.conf";
	
	/** folder for downloading files */
	private static String downloadFolder;
	
	/** ESB endpoint address */
	private static String endpointAddress;
	
	/** paths and passwords for trust and keystore */
	private static String trustorePath;
	private static String trustorePassword;
	private static String keystorePath;
	private static String keystorePassword;
	
	private static String dailyPath;
	private static String hourlyPath;
	
	private static Map<String, String> configurations = new HashMap<>();
	
	private static FTPClient ftpClient = new FTPClient();

	public static void main(String[] args) throws Exception {
		
		try {
			
			if (args.length == 0) {
				log.info("You need to pass arguement DAILY or HOURLY");
				System.exit(0);
			}
			
			loadConfigurations();
			initVariables();
			String fetchPath = "";
			
			if (args[0].equals("DAILY")) {
				fetchPath = dailyPath;
				log.info("Started ARSO data fetch: " + args[0]);
			} else {
				fetchPath = hourlyPath;
				log.info("Started ARSO data fetch: " + args[0]);
			}
			
			downloadFolder = CreateFolders.createDirectory(downloadFolder);

			//connect to FTP
			ftpClient.connect("ftp.arso.gov.si", 21);
			boolean login = ftpClient.login(configurations.get("username"), configurations.get("password"));
			log.info("Login successful: " + login);
			
			ftpClient.setFileType(FTP.BINARY_FILE_TYPE);
			ftpClient.enterLocalPassiveMode();
			
			//all files in FTP
			FTPFile[] files = ftpClient.listFiles(fetchPath);
			log.info("Files size: " + files.length);
			
			//scheduler is running - export only the latest file
			Arrays.sort(files, Comparator.comparing((FTPFile remoteFile) -> remoteFile.getTimestamp()).reversed());
			FTPFile latestFile = files[0];
			log.info("Newest file is " + latestFile.getName() + " with timestamp " + latestFile.getTimestamp().getTime().toString());
			downloadFile(ftpClient, fetchPath + latestFile.getName(), downloadFolder + File.separator + latestFile.getName());

		} catch (IOException e) {
			log.error("ftp client error ", e);
		} finally {
			try {
				ftpClient.disconnect();
			} catch (IOException e) {
				log.error("ftp disconnect client error ", e);
			}
		}
	}
	
	/** initialize variables from configuration */
	private static void initVariables() {
		downloadFolder =  configurations.get("downloadFolder");
		endpointAddress = configurations.get("endpointAddress");
		trustorePath = configurations.get("trustorePath");
		trustorePassword = configurations.get("trustorePassword");
		keystorePath = configurations.get("keystorePath");
		keystorePassword = configurations.get("keystorePassword");
		dailyPath = configurations.get("dailyPath");
		hourlyPath = configurations.get("hourlyPath");
	}

	/** load info from config file */
	private static void loadConfigurations() throws IOException {
		String configString = readFile(CONFIG_PATH);
		
		String [] configLines = configString.split(";");
		
		for (String string : configLines) {
			string = string.replace(System.getProperty("line.separator"), "");
			String [] configAttributes = string.split("=");
			configurations.put(configAttributes[0], configAttributes[1]);
		}
		
	}

	/** download single file 
	 * @throws Exception */
	private static void downloadFile(FTPClient ftpClient, String inputPath, String outputPath) throws Exception {

		String remoteFile1 = inputPath;
		File downloadFile1 = new File(outputPath);
		OutputStream outputStream1 = new BufferedOutputStream(new FileOutputStream(downloadFile1));
		boolean success = ftpClient.retrieveFile(remoteFile1, outputStream1);
		log.info("File downloaded: " + success + "; Source: " + inputPath  + "; Target: " + outputPath);
		outputStream1.close();

		//convert message to String
		getStringMessageAndConvertToJava(outputPath);
	}
	
	private static void getStringMessageAndConvertToJava(String outputPath) throws Exception {
		
		String message = readFile(outputPath);
		String weatherType = "";
		
		if (outputPath.contains("_forecast_")) {
			weatherType = ELEnvironmentalInformationKind.FORECAST.toString();
		} else if (outputPath.contains("_anal_")) {
			weatherType = ELEnvironmentalInformationKind.OBSERVATION.toString();
		}		
		//unmarshall and send to ESB
		XMLUnmarshaller.convertToJava(message, weatherType, endpointAddress, trustorePath, trustorePassword, keystorePath, keystorePassword);
	}

	/** convert file to String */
	public static String readFile(String fpath) throws IOException {
		byte[] encoded = Files.readAllBytes(Paths.get(fpath));
		return new String(encoded, "UTF-8");
	}
	
}
