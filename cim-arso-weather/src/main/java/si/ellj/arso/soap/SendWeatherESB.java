package si.ellj.arso.soap;

import java.time.LocalDateTime;
import java.util.GregorianCalendar;
import java.util.List;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.log4j.Logger;

import ch.iec.tc57._2011.schema.message.HeaderType;
import ch.iec.tc57._2011.sendweather.FaultMessage;
import ch.iec.tc57._2011.weathermessage.WeatherEventMessageType;
import ch.iec.tc57._2011.weathermessage.WeatherPayloadType;
import ch.iec.tc57._2011.weathermessage.WeatherResponseMessageType;
import ch.iec.tc57._2018._7.weather_.AnalogValue;
import ch.iec.tc57._2018._7.weather_.AtmosphericAnalog;
import ch.iec.tc57._2018._7.weather_.AtmosphericAnalogKind;
import ch.iec.tc57._2018._7.weather_.ELEnvironmentalInformation;
import ch.iec.tc57._2018._7.weather_.ELEnvironmentalInformationKind;
import ch.iec.tc57._2018._7.weather_.ELWeatherRegion;
import ch.iec.tc57._2018._7.weather_.EnvironmentalDataProvider;
import ch.iec.tc57._2018._7.weather_.MeasurementValueQuality;
import ch.iec.tc57._2018._7.weather_.Validity;
import ch.iec.tc57._2018._7.weather_.Weather;
import si.ellj.arso.esb.ESBClient;
import si.ellj.arso.object.ArsoDataObject;
import si.ellj.arso.object.MetData;

public class SendWeatherESB {
	
	private static final Logger log = Logger.getLogger(SendWeatherESB.class);

	public static void sendWeatherToESB(ArsoDataObject arsoObject, String weatherType, String endpointAddress, String trustorePath, String trustorePassword, String keystorePath, String keystorePassword) throws DatatypeConfigurationException, FaultMessage {
				
		List<MetData> metDataList = arsoObject.getMetData();
		
		for (MetData metData : metDataList) {
			
			//create message for every region
			WeatherEventMessageType weatherRequestMessageType = new WeatherEventMessageType();
			WeatherPayloadType weatherPayloadType = new WeatherPayloadType();
			Weather weather = new Weather();
			
			//convert metData to ELEnvironmentalInformation
			ELEnvironmentalInformation environmentalInformation = new ELEnvironmentalInformation();
			
			//meteosiID - location; description -> coordinates
			ELWeatherRegion weatherRegion = new ELWeatherRegion();
			weatherRegion.setName(metData.getDomain_longTitle());
			weatherRegion.setDescription("lat: " + metData.getDomain_lat() + ", lon: " + metData.getDomain_lon());
			environmentalInformation.getELWeatherRegions().add(weatherRegion);
		
			//create new object for every type
			AtmosphericAnalog temperature = setTemperatrueNode(metData);
			AtmosphericAnalog humidity = setHumidityNode(metData);
			AtmosphericAnalog sunRad = setSunRadiationNode(metData);
			AtmosphericAnalog windSpeed = setWindSpeedNode(metData);
			AtmosphericAnalog precipication = setPrecipitatonNode(metData);

			environmentalInformation.getEnvironmentalAnalog().add(temperature);
			environmentalInformation.getEnvironmentalAnalog().add(humidity);
			environmentalInformation.getEnvironmentalAnalog().add(sunRad);
			environmentalInformation.getEnvironmentalAnalog().add(windSpeed);
			environmentalInformation.getEnvironmentalAnalog().add(precipication);
			
			//created dateTime
			environmentalInformation.setCreated(metData.getValid_UTC());
			
			//weather type - forecast or actual data
			if (weatherType.equals("FORECAST")) {
				environmentalInformation.setElEnvironmentalInformationKind(ELEnvironmentalInformationKind.FORECAST);				
			} else {
				environmentalInformation.setElEnvironmentalInformationKind(ELEnvironmentalInformationKind.OBSERVATION);
			}

			EnvironmentalDataProvider environmentalDataProvider = new EnvironmentalDataProvider();
			environmentalDataProvider.setName("ARSO");
			
			environmentalInformation.setEnvironmentalDataProvider(environmentalDataProvider);
			weather.getELEnvironmentalInformation().add(environmentalInformation);
			
			//set weather, payload and header
			weatherPayloadType.setWeather(weather);
			weatherRequestMessageType.setPayload(weatherPayloadType);
			weatherRequestMessageType.setHeader(setHeader(weatherType));
			
			//send to ESB for every Region
		    ESBClient esbClient = new ESBClient(endpointAddress, trustorePath, trustorePassword, keystorePath, keystorePassword);
		    WeatherResponseMessageType weatherReponseMessageType = esbClient.changedWeather(weatherRequestMessageType);
		    log.info("ESB response: " + weatherReponseMessageType.getReply().getResult());
		}
	}
	
	/** set attributes for precipitation */
	private static AtmosphericAnalog setPrecipitatonNode(MetData metData) {
		
		AtmosphericAnalog atmosphericAnalog = new AtmosphericAnalog();
		atmosphericAnalog.setUnitMultiplier("m");
		atmosphericAnalog.setUnitSymbol("m");
		atmosphericAnalog.setKind(AtmosphericAnalogKind.PRECIPITATION);
		
		AnalogValue analogValue = new AnalogValue();
		analogValue.setValue(metData.getTp_1h_acc());

		MeasurementValueQuality measurementValueQuality = setMeasurementQuality();
		analogValue.setMeasurementValueQuality(measurementValueQuality);
		
		atmosphericAnalog.getAnalogValues().add(analogValue);
		
		return atmosphericAnalog;
		
	}

	private static MeasurementValueQuality setMeasurementQuality() {
		MeasurementValueQuality measurementValueQuality = new MeasurementValueQuality();
		measurementValueQuality.setValidity(Validity.GOOD);
		return measurementValueQuality;
	}

	/** set attributes for wind speed */
	private static AtmosphericAnalog setWindSpeedNode(MetData metData) {
		
		AtmosphericAnalog atmosphericAnalog = new AtmosphericAnalog();
		atmosphericAnalog.setUnitMultiplier("none");
		atmosphericAnalog.setUnitSymbol("mPers");
		atmosphericAnalog.setKind(AtmosphericAnalogKind.WIND_SPEED_SUSTAINED);
		
		AnalogValue analogValue = new AnalogValue();
		
		// m/s -> km/h
		analogValue.setValue(metData.getFf_val() * 3.6f);
		
		MeasurementValueQuality measurementValueQuality = setMeasurementQuality();
		analogValue.setMeasurementValueQuality(measurementValueQuality);
		
		atmosphericAnalog.getAnalogValues().add(analogValue);
		
		return atmosphericAnalog;
		
	}

	/** set attributes for sun radiation */
	private static AtmosphericAnalog setSunRadiationNode(MetData metData) {
		
		AtmosphericAnalog atmosphericAnalog = new AtmosphericAnalog();
		atmosphericAnalog.setUnitMultiplier("none");
		atmosphericAnalog.setUnitSymbol("WPerm2");
		atmosphericAnalog.setKind(AtmosphericAnalogKind.IRRADIANCE_DIRECT_NORMAL);
		
		AnalogValue analogValue = new AnalogValue();
		analogValue.setValue(metData.getgSunRad());
		
		MeasurementValueQuality measurementValueQuality = setMeasurementQuality();
		analogValue.setMeasurementValueQuality(measurementValueQuality);
		
		atmosphericAnalog.getAnalogValues().add(analogValue);
		
		return atmosphericAnalog;
		
	}

	/** set attributes for humidity */
	private static AtmosphericAnalog setHumidityNode(MetData metData) {
		
		AtmosphericAnalog atmosphericAnalog = new AtmosphericAnalog();
		atmosphericAnalog.setUnitMultiplier("none");
		atmosphericAnalog.setUnitSymbol("none");
		atmosphericAnalog.setKind(AtmosphericAnalogKind.HUMIDITY);
		
		AnalogValue analogValue = new AnalogValue();
		analogValue.setValue(metData.getRh());
		
		MeasurementValueQuality measurementValueQuality = setMeasurementQuality();
		analogValue.setMeasurementValueQuality(measurementValueQuality);
		
		atmosphericAnalog.getAnalogValues().add(analogValue);
		
		return atmosphericAnalog;
		
	}

	/** set attributes for temperature */
	private static AtmosphericAnalog setTemperatrueNode(MetData metData) {
		
		AtmosphericAnalog atmosphericAnalog = new AtmosphericAnalog();
		atmosphericAnalog.setUnitMultiplier("none");
		atmosphericAnalog.setUnitSymbol("degC");
		atmosphericAnalog.setKind(AtmosphericAnalogKind.AMBIENT_TEMPERATURE);
		
		AnalogValue analogValue = new AnalogValue();
		analogValue.setValue(metData.getT());
		
		MeasurementValueQuality measurementValueQuality = setMeasurementQuality();
		analogValue.setMeasurementValueQuality(measurementValueQuality);
		
		atmosphericAnalog.getAnalogValues().add(analogValue);
		
		return atmosphericAnalog;
		
	}

	/** set header attributes 
	 * @throws DatatypeConfigurationException */
	private static HeaderType setHeader(String weatherType) throws DatatypeConfigurationException {
		
		HeaderType headerType = new HeaderType();

		if (weatherType.equals("FORECAST")) {
			headerType.setNoun("ForecastWeatherData");
		} else {
			headerType.setNoun("ActualWeatherData");
		}
		
		headerType.setVerb("changed");
		headerType.setRevision("2.0");
		
		GregorianCalendar gregorianCalendar = new GregorianCalendar();
		gregorianCalendar.setTimeInMillis(System.currentTimeMillis());
		LocalDateTime dateTime = LocalDateTime.now();
		
		XMLGregorianCalendar xmlGregorianCalendar = DatatypeFactory.newInstance().newXMLGregorianCalendar(gregorianCalendar);
		headerType.setTimestamp(xmlGregorianCalendar);
		headerType.setMessageID(dateTime.getYear() + "_" + dateTime.getMonthValue() + "_" + dateTime.getDayOfMonth() + "_" + dateTime.getHour() + "_" + dateTime.getMinute() + "_" + dateTime.getSecond() + "_" + dateTime.getNano());
		headerType.setCorrelationID(dateTime.getYear() + "_" + dateTime.getMonthValue() + "_" + dateTime.getDayOfMonth() + "_" + dateTime.getHour() + "_" + dateTime.getMinute() + "_" + dateTime.getSecond() + "_" + dateTime.getNano());
		
		return headerType;
	}
}
