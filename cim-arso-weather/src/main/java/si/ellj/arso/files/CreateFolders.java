package si.ellj.arso.files;

import java.io.File;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class CreateFolders {

	/** formated dateTimes and file names */
	private static String formattedDateTime = "";
	private static String formattedDate = "";
	private static String folderName = "";
	
	public static String createDirectory(String path) {

		setDateString();
		folderName = path + File.separator + formattedDate;

		File directory = new File(folderName);
		
		//if folder for this month does not exist, create it
		if (!directory.exists()) {
			directory.mkdir();
		}
		
		folderName = createSeparateFolders();
		return folderName;

	}
	
	/** get date and time and convert to String */
	private static void setDateString() {

		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd-HH-mm");
		DateTimeFormatter dtf2 = DateTimeFormatter.ofPattern("yyyy-MM");

		LocalDateTime dateTime = LocalDateTime.now();

		formattedDateTime = dtf.format(dateTime);
		formattedDate = dtf2.format(dateTime);
	}
	
	/** create sub directories according to date and time */ 
	private static String createSeparateFolders() {

		String subFolderDateTime = folderName + File.separator + "ARSO-" + formattedDateTime;
		File directoryDateTime  = new File(subFolderDateTime);
		createSubDirectory(directoryDateTime);
		return subFolderDateTime;
	}

	/** create sub directory */
	private static void createSubDirectory(File directory) {
		if (!directory.exists()) {
			directory.mkdir();
		}
	}
}
