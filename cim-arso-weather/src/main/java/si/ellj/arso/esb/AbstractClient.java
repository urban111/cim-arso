package si.ellj.arso.esb;

import java.util.HashMap;
import java.util.Map;

import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;

public abstract class AbstractClient implements Client {

	public static final String PROP_ADDRESS = "address";

	protected Object client;

	public abstract String getAddress();

	Map<String, Object> properties = new HashMap<String, Object>();

	@Override
	public void init() throws Exception {
		JaxWsProxyFactoryBean factory = new JaxWsProxyFactoryBean();
		factory.setServiceClass(getServiceClass());
		factory.setAddress(getAddress());
		client = factory.create();

	}
}
