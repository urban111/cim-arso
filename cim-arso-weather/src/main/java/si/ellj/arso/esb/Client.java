package si.ellj.arso.esb;

public interface Client {
  
    Class<?> getServiceClass();
    void init() throws Exception;
}
