package si.ellj.arso.esb;

import java.io.File;
import java.io.FileInputStream;
import java.security.KeyStore;
import java.security.cert.X509Certificate;

import javax.net.ssl.KeyManager;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

import org.apache.cxf.configuration.jsse.TLSClientParameters;
import org.apache.cxf.frontend.ClientProxy;
import org.apache.cxf.transport.http.HTTPConduit;
import org.apache.log4j.Logger;

import ch.iec.tc57._2011.sendweather.FaultMessage;
import ch.iec.tc57._2011.sendweather.WeatherPort;
import ch.iec.tc57._2011.weathermessage.WeatherEventMessageType;
import ch.iec.tc57._2011.weathermessage.WeatherResponseMessageType;

public class ESBClient extends AbstractClient implements WeatherPort {

	private TrustManager[] tm;
	private KeyManager[] km;
	private boolean isHttpSecureEnabled = true;

	String NAME = "ESBSendWeather";

	String trustorePath;
	String trustorePassword;
	String keystorePath;
	String keystorePassword;
	String PROP_ADAPTER_ADDRESS;

	private static final Logger log = Logger.getLogger(ESBClient.class);

	public ESBClient(String enpointAddress, String trustorePath, String trustorePassword, String keystorePath, String keystorePassword) {
		this.trustorePath = trustorePath;
		this.trustorePassword = trustorePassword;
		this.keystorePassword = keystorePassword;
		this.keystorePath = keystorePath;
		PROP_ADAPTER_ADDRESS = enpointAddress;
	}
	
	public void start() throws Exception {

		if (isHttpSecureEnabled) {
			KeyStore trustore = KeyStore.getInstance("JKS");
			File truststoreFile = new File(trustorePath);
			trustore.load(new FileInputStream(truststoreFile), trustorePassword.toCharArray());
			TrustManagerFactory trustFactory = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
			trustFactory.init(trustore);
			tm = trustFactory.getTrustManagers();

			KeyStore keystore = KeyStore.getInstance("JKS");
			File keystoreFile = new File(keystorePath);
			keystore.load(new FileInputStream(keystoreFile), keystorePassword.toCharArray());
			KeyManagerFactory keyFactory = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
			keyFactory.init(keystore, keystorePassword.toCharArray());
			km = keyFactory.getKeyManagers();
		}
		
		init();
	}

	@Override
	public Class<?> getServiceClass() {
		return WeatherPort.class;
	}

	private WeatherPort getClient() {
		return (WeatherPort) client;
	}

	private void setHttpConduit() {
		org.apache.cxf.endpoint.Client clientSSL = ClientProxy.getClient(client);
		HTTPConduit httpConduit = (HTTPConduit) clientSSL.getConduit();
		TLSClientParameters tlsParams = new TLSClientParameters();
		tlsParams.setDisableCNCheck(true);
		//tlsParams.setTrustManagers(tm);
		tlsParams.setTrustManagers(new TrustManager[] { new TrustAllX509TrustManager() });
		tlsParams.setKeyManagers(km);
		httpConduit.setTlsClientParameters(tlsParams);
	}

	@Override
	public String getAddress() {
		return PROP_ADAPTER_ADDRESS;
	}

	/**
	 * This class allow any X509 certificates to be used to authenticate the
	 * remote side of a secure socket, including self-signed certificates.
	 */
	public static class TrustAllX509TrustManager implements X509TrustManager {

		private static final X509Certificate[] acceptedIssuers = new X509Certificate[] {};

		public void checkClientTrusted(X509Certificate[] chain, String authType) {
		}

		public void checkServerTrusted(X509Certificate[] chain, String authType) {
		}

		public X509Certificate[] getAcceptedIssuers() {
			return (acceptedIssuers);
		}
	}

	@Override
	public WeatherResponseMessageType deletedWeather(WeatherEventMessageType deletedWeatherEventMessage) throws FaultMessage {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public WeatherResponseMessageType changedWeather(WeatherEventMessageType changedWeatherEventMessage) throws FaultMessage {
		try {
			start();
			setHttpConduit();

			return getClient().changedWeather(changedWeatherEventMessage);
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e);
		}
		return null;

	}

	@Override
	public WeatherResponseMessageType createdWeather(WeatherEventMessageType createdWeatherEventMessage) throws FaultMessage {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public WeatherResponseMessageType canceledWeather(WeatherEventMessageType canceledWeatherEventMessage) throws FaultMessage {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public WeatherResponseMessageType closedWeather(WeatherEventMessageType closedWeatherEventMessage) throws FaultMessage {
		// TODO Auto-generated method stub
		return null;
	}

}
