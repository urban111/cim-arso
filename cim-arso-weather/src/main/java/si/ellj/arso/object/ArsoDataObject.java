package si.ellj.arso.object;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "data")
@XmlAccessorType(XmlAccessType.PROPERTY)
public class ArsoDataObject implements Serializable {

	private static final long serialVersionUID = 1L;

    @XmlElement(name = "metData")
    protected List<MetData> metData;
    
    public List<MetData> getMetData() {
        if (metData == null) {
        	metData = new ArrayList<MetData>();
        }
        return this.metData;
    }
   
}
