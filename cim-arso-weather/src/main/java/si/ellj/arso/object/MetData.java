package si.ellj.arso.object;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.datatype.XMLGregorianCalendar;

@XmlRootElement(name = "metData")
@XmlAccessorType(XmlAccessType.PROPERTY)
public class MetData implements Serializable {

	private static final long serialVersionUID = 1L;

	private String domain_longTitle;
	private String domain_meteosiId;
	private XMLGregorianCalendar tsUpdated_UTC;
	private XMLGregorianCalendar valid_UTC;
	private float domain_lat;
	private float domain_lon;
	private float t;
	private float ff_val;
	private float dd_val;
	private float tp_1h_acc;
	private float rh;
	private float gSunRad;

	public MetData() {
		super();
	}

	public String getDomain_longTitle() {
		return domain_longTitle;
	}

	public void setDomain_longTitle(String domain_longTitle) {
		this.domain_longTitle = domain_longTitle;
	}

	public String getDomain_meteosiId() {
		return domain_meteosiId;
	}

	public void setDomain_meteosiId(String domain_meteosiId) {
		this.domain_meteosiId = domain_meteosiId;
	}

	public XMLGregorianCalendar getTsUpdated_UTC() {
		return tsUpdated_UTC;
	}

	public void setTsUpdated_UTC(XMLGregorianCalendar tsUpdated_UTC) {
		this.tsUpdated_UTC = tsUpdated_UTC;
	}

	public XMLGregorianCalendar getValid_UTC() {
		return valid_UTC;
	}

	public void setValid_UTC(XMLGregorianCalendar valid_UTC) {
		this.valid_UTC = valid_UTC;
	}

	public float getDomain_lat() {
		return domain_lat;
	}

	public void setDomain_lat(float domain_lat) {
		this.domain_lat = domain_lat;
	}

	public float getDomain_lon() {
		return domain_lon;
	}

	public void setDomain_lon(float domain_lon) {
		this.domain_lon = domain_lon;
	}

	public float getT() {
		return t;
	}

	public void setT(float t) {
		this.t = t;
	}

	public float getFf_val() {
		return ff_val;
	}

	public void setFf_val(float ff_val) {
		this.ff_val = ff_val;
	}

	public float getDd_val() {
		return dd_val;
	}

	public void setDd_val(float dd_val) {
		this.dd_val = dd_val;
	}

	public float getTp_1h_acc() {
		return tp_1h_acc;
	}

	public void setTp_1h_acc(float tp_1h_acc) {
		this.tp_1h_acc = tp_1h_acc;
	}

	public float getRh() {
		return rh;
	}

	public void setRh(float rh) {
		this.rh = rh;
	}

	public float getgSunRad() {
		return gSunRad;
	}

	public void setgSunRad(float gSunRad) {
		this.gSunRad = gSunRad;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
